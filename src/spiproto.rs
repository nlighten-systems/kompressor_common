use core::ops::{Deref, DerefMut};

use int_enum::IntEnum;

#[repr(u8)]
#[derive(IntEnum, Hash, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Dest {
    SX130X = 0,
    Radio1 = 1,
    Radio2 = 2,
    SMCU = 3,
    Host = 4,
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct PktHeader {
    pub dest: Dest,
    pub addr: u16,
    pub len: u8,
    pub write: bool,
    pub read: bool,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    BadDest(u8),
}

const CMD_W_BIT: u8 = 0x08;
const CMD_R_BIT: u8 = 0x10;

impl PktHeader {
    pub fn write_header(&self, buf: &mut Packet) {
        let buf_size = self.len - 1;
        let mut bits = 0_u8;
        if self.write {
            bits |= CMD_W_BIT;
        }
        if self.read {
            bits |= CMD_R_BIT;
        }
        buf[0] = bits | (0xC0 & (buf_size << 6)) | (0x07 & self.dest as u8);
        buf[1] = (0x00FF & self.addr) as u8;
        buf[2] = (0x007F & (self.addr >> 8)) as u8 | ((0x04 & buf_size) << 5);
    }

    pub fn from_bytes(buf: &[u8]) -> Result<Self, Error> {
        let retval = Self {
            dest: Dest::from_int(0x07 & buf[0]).map_err(|_| Error::BadDest(buf[0]))?,
            addr: (0x7F00 & ((buf[2] as u16) << 8)) | (0x00FF & buf[1] as u16),
            len: ((0x04 & (buf[2] >> 5)) | (0x03 & (buf[0] >> 6))) + 1,
            write: buf[0] & CMD_W_BIT > 0,
            read: buf[0] & CMD_R_BIT > 0,
        };

        Ok(retval)
    }
}

const PKT_SIZE: usize = 11;

#[derive(Debug, Default, Clone, Copy)]
pub struct Packet([u8; PKT_SIZE]);

impl Packet {
    pub const PAYLOAD_SIZE: usize = PKT_SIZE - 3;

    pub fn write_payload(&mut self, payload: &[u8]) {
        for i in 0..payload.len() {
            self.0[3 + i] = payload[i];
        }
    }
}

impl Deref for Packet {
    type Target = [u8; PKT_SIZE];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Packet {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
