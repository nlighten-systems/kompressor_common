use crate::lib::*;

#[allow(non_camel_case_types)]
#[repr(u8)]
#[derive(IntEnum, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum CodingRate {
    CR_4_5 = 1,
    CR_4_6 = 2,
    CR_4_7 = 3,
    CR_4_8 = 4,
}
