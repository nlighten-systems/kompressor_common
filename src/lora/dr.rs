use crate::lib::*;

use super::{Bandwidth, SpreadingFactor};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Datarate(pub SpreadingFactor, pub Bandwidth);

impl Datarate {
    pub fn try_from<T>(value: T) -> Result<Self, &'static str>
    where
        T: AsRef<[u8]>,
    {
        use safe_regex::{regex, Matcher2};
        let matcher: Matcher2<_> = regex!(br"SF([0-9]+)BW([1|2|5|0]{3})");
        match matcher.match_slices(value.as_ref()) {
            Some((sf, bw)) => {
                use core::str;

                if let (Ok(sf), Ok(bw)) = (
                    SpreadingFactor::from_int(unsafe {
                        str::from_utf8_unchecked(sf).parse::<u8>().unwrap()
                    }),
                    match bw {
                        br"125" => Ok(Bandwidth::BW125),
                        br"250" => Ok(Bandwidth::BW250),
                        br"500" => Ok(Bandwidth::BW500),
                        _ => Err(""),
                    },
                ) {
                    Ok(Datarate(sf, bw))
                } else {
                    Err("")
                }
            }
            None => Err("e"),
        }
    }

    pub fn to_string(&self) -> SmallString {
        use core::fmt::Write;

        let mut fmt = SmallString::new();
        let _ = write!(&mut fmt, "{:?}{:?}", self.0, self.1);
        fmt
    }
}

impl uDisplay for Datarate {
    fn fmt<W>(&self, formatter: &mut ufmt::Formatter<W>) -> Result<(), W::Error>
    where
        W: uWrite + ?Sized,
    {
        uwrite!(formatter, "{:?}{:?}", self.0, self.1)
    }
}

impl Display for Datarate {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:?}{:?}", self.0, self.1)
    }
}

impl serde::ser::Serialize for Datarate {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        use core::fmt::Write;
        //let mut buf = [0_u8;10];
        //let mut fmt = FixedString::new(&mut buf);
        let mut fmt = SmallString::new();
        write!(&mut fmt, "{}", self)
            .map_err(|e| serde::ser::Error::custom(format_args!("{}", e)))?;
        serializer.serialize_str(fmt.as_str())
    }
}

#[cfg(feature = "alloc")]
impl<'de> serde::de::Deserialize<'de> for Datarate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct MyStrVisitor;

        impl<'de> serde::de::Visitor<'de> for MyStrVisitor {
            type Value = Datarate;

            fn expecting(&self, formatter: &mut alloc::fmt::Formatter) -> alloc::fmt::Result {
                formatter.write_str("a string in the from SFxxBWxxx")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Datarate::try_from(v.as_bytes())
                    .map_err(|e| E::custom(format_args!("cannot parse {} as datarate", v)))
            }
        }

        deserializer.deserialize_str(MyStrVisitor)
    }
}
