use crate::lib::*;

#[repr(u8)]
#[derive(IntEnum, Debug, uDebug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[cfg_attr(feature = "alloc", derive(Deserialize))]
pub enum SpreadingFactor {
    SF5 = 5,
    SF6 = 6,
    SF7 = 7,
    SF8 = 8,
    SF9 = 9,
    SF10 = 10,
    SF11 = 11,
    SF12 = 12,
}

impl SpreadingFactor {
    pub fn get_factor(&self) -> u8 {
        *self as u8
    }
}
