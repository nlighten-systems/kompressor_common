use crate::lib::*;

#[repr(u8)]
#[derive(IntEnum, Debug, uDebug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Bandwidth {
    BW125 = 4,
    BW250 = 5,
    BW500 = 6,
}

impl Bandwidth {
    pub fn from_hz(hz: u32) -> Result<Bandwidth, Error> {
        match hz {
            125_000 => Ok(Bandwidth::BW125),
            250_000 => Ok(Bandwidth::BW250),
            500_000 => Ok(Bandwidth::BW500),
            _ => Err(Error::Unknown),
        }
    }

    pub fn get_bandwidth_hz(&self) -> u32 {
        match *self {
            Bandwidth::BW125 => 125_000,
            Bandwidth::BW250 => 250_000,
            Bandwidth::BW500 => 500_000,
        }
    }
}
