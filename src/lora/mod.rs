mod bw;
pub use bw::Bandwidth;

mod cr;
pub use cr::CodingRate;

mod dr;
pub use dr::Datarate;

mod sf;
pub use sf::SpreadingFactor;

use crate::lib::*;

use crate::gps::{GPSTime, WGS84Position};

pub use macaddr::MacAddr8 as MacAddress;

pub trait RxPktRep {
    fn gps_time(&self) -> Option<GPSTime>;

    /// Frequency (hertz)
    fn freq(&self) -> u32;

    fn datarate(&self) -> Datarate;

    /// Signal to Noise radio (0.01dB)
    fn snr(&self) -> i32;

    /// Received Signal Strength Indicator (1 dBm)
    fn rssi(&self) -> u8;

    /// Internal timestamp of "RX finished" event (32b unsigned)
    fn tmst(&self) -> u32;

    fn gatwy_id(&self) -> MacAddress;
    fn pos(&self) -> Option<WGS84Position>;
    fn payload(&self) -> &[u8];

    fn write_message<T: crate::io::Write>(&self, writer: &mut T) -> crate::io::Result<()> {
        if let Some(time) = self.gps_time() {
            let duration = time.as_duration();
            writer.write_all(&duration.as_secs().to_be_bytes())?;
            writer.write_all(&duration.subsec_nanos().to_be_bytes())?;
        }

        writer.write_all(&self.freq().to_be_bytes())?;
        writer.write_all(self.datarate().to_string().as_bytes())?;
        writer.write_all(&self.snr().to_be_bytes())?;
        writer.write_all(&self.rssi().to_be_bytes())?;
        writer.write_all(&self.tmst().to_be_bytes())?;
        writer.write_all(&self.gatwy_id().as_bytes())?;
        if let Some(ref pos) = self.pos() {
            writer.write_all(&pos.lat.to_be_bytes())?;
            writer.write_all(&pos.lon.to_be_bytes())?;
            writer.write_all(&pos.height.to_be_bytes())?;
        }
        writer.write_all(self.payload())?;
        Ok(())
    }

    #[cfg(feature = "crypto")]
    fn as_hash(&self) -> [u8; Sha512::SHA512_HASH_SIZE] {
        let mut hash = Sha512::new();
        let _ = self.write_message(&mut hash);
        hash.finish()
    }

    #[cfg(feature = "crypto")]
    fn sign(&self, key: &PrivateKey) -> Signature {
        key.sign(self.as_hash())
    }

    #[cfg(feature = "crypto")]
    fn verify(&self, key: &PublicKey, sig: &Signature) -> bool {
        key.verify(self.as_hash(), sig)
    }
}

pub struct RxPkt<'a> {
    pub gps_time: Option<GPSTime>,
    pub freq: u32,
    pub datarate: Datarate,
    pub snr: i32,
    pub rssi: u8,
    pub tmst: u32,
    pub gatwy_id: MacAddress,
    pub pos: Option<WGS84Position>,
    pub payload: &'a [u8],
}

impl<'a> RxPkt<'a> {}

impl<'a> RxPktRep for RxPkt<'a> {
    fn gps_time(&self) -> Option<GPSTime> {
        self.gps_time
    }

    fn freq(&self) -> u32 {
        self.freq
    }

    fn datarate(&self) -> Datarate {
        self.datarate
    }

    fn snr(&self) -> i32 {
        self.snr
    }

    fn rssi(&self) -> u8 {
        self.rssi
    }

    fn tmst(&self) -> u32 {
        self.tmst
    }

    fn gatwy_id(&self) -> MacAddress {
        self.gatwy_id
    }

    fn pos(&self) -> Option<WGS84Position> {
        self.pos
    }

    fn payload(&self) -> &[u8] {
        &self.payload
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_datarate() {
        let dr = Datarate::try_from(br"SF7BW125");
        assert_eq!(
            dr.unwrap(),
            Datarate(SpreadingFactor::SF7, Bandwidth::BW125)
        );
    }

    #[test]
    fn test_write_message() {
        use hex::ToHex;

        let pkt = RxPkt {
            gps_time: Some(GPSTime::new(2000, 100, 0)),
            freq: 904_000_000,
            datarate: Datarate(SpreadingFactor::SF7, Bandwidth::BW125),
            snr: -100,
            rssi: 100,
            tmst: 10_000,
            gatwy_id: MacAddress::nil(),
            pos: None,
            payload: b"hello world".as_slice(),
        };

        let hash = pkt.as_hash();
        assert_eq!(hash.encode_hex::<String>(), "2614597804564bb0386e07fe0aa5212244127d9776bac874696741416acc1dbf6d4b61a0abb62b90fa02a11da6c092be2c4f90af903a39f1e688b4a7c55498de");
    }
}
