use crate::gps::WGS84Position;
use crate::lib::*;
use crate::lora::Datarate;

use serde::{
    de::Deserialize,
    ser::{Serialize, SerializeMap},
};

use ed25519_dalek::{PublicKey, Signature, Verifier};

use bytes::{BufMut, BytesMut};

pub struct LoraPktRep {
    pub time_utc: String,
    pub freq: u32,
    pub datarate: Datarate,
    pub snr: i32,
    pub rssi: u8,
    pub tmst: u32,
    pub gatwy_id: u64,
    pub pos: Option<WGS84Position>,
    pub payload: Box<[u8]>,
}

impl LoraPktRep {
    pub fn verify(&self, sig: &Signature, pub_key: &PublicKey) -> Result<(), Error> {
        // let buf = BytesMut::with_capacity(1024);
        // let mut writer = buf.writer();
        // ciborium::ser::into_writer(self, &mut writer).map_err(|_| Error::Unknown)?;

        // let buf = writer.into_inner();

        // pub_key.verify(&buf[..], sig).map_err(|_| Error::Unknown)
        todo!()
    }
}

impl Serialize for LoraPktRep {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut m = serializer.serialize_map(None)?;

        m.serialize_entry("time_utc", &self.time_utc)?;
        m.serialize_entry("freq", &self.freq)?;
        m.serialize_entry("datarate", &self.datarate)?;
        m.serialize_entry("snr", &self.snr)?;
        m.serialize_entry("rssi", &self.rssi)?;
        m.serialize_entry("tmst", &self.tmst)?;
        m.serialize_entry("gatwy_id", &self.gatwy_id)?;

        if let Some(ref position) = self.pos {
            m.serialize_entry("pos", position);
        }

        m.serialize_entry("payload", &self.payload)?;
        m.end()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_verify() {
        let sig = Signature;
        let pub_key = PublicKey;
        let pkt = LoraPktRep {};

        assert!(pkt.verify(&sig, &pub_key));
    }
}
