#![deny(warnings)]
#![cfg_attr(not(test), no_std)]

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "crypto")]
pub mod crypto;

#[cfg(feature = "lora")]
pub mod lora;

mod lib {

    pub use log::{debug, error, info, trace, warn};

    pub use core::fmt::{Debug, Display};

    #[cfg(feature = "alloc")]
    pub use alloc::{
        boxed::Box,
        string::{String, ToString},
    };

    pub use int_enum::IntEnum;

    pub use crate::Error;

    pub use ufmt::{derive::uDebug, uDisplay, uWrite, uwrite};

    pub type SmallString = heapless::String<16>;

    #[cfg(feature = "crypto")]
    pub use crate::crypto::{ed25519::*, sha512::Sha512};
}

pub mod prelude {
    pub use crate::lib::*;
}

#[derive(Debug)]
pub enum Error {
    Unknown,
}

pub mod gps;
pub mod io;
pub mod pps_clock;
pub mod spiproto;
pub mod utils;

#[cfg(feature = "verify")]
pub mod signing;
