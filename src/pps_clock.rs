#[derive(Debug)]
pub enum Error {
    NoRef,
}

#[derive(Debug, Clone)]
pub struct PPSClock {
    last: Option<u32>,
    clock_per_sec: u32,
}

impl PPSClock {
    pub fn new() -> Self {
        Self {
            last: None,
            clock_per_sec: 0,
        }
    }

    pub fn last_pps(&mut self, clock: u32) {
        if let Some(last) = self.last {
            if last == clock {
                return;
            }
            self.clock_per_sec = clock.wrapping_sub(last);
        }
        self.last = Some(clock);
    }

    pub fn to_ns(&self, clock: u32) -> Result<i32, Error> {
        let last = self.last.ok_or(Error::NoRef)?;
        let diff = clock.wrapping_sub(last);

        let diff = match diff {
            diff if diff > u32::MAX / 2 => ((diff - 1) as i64 - u32::MAX as i64) as i32,
            diff => diff as i32,
        };

        let v = 1_000_000_000_i64 * diff as i64 / self.clock_per_sec as i64;
        Ok(v as i32)
    }

    pub fn clocks_per_sec(&self) -> u32 {
        self.clock_per_sec
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_clock() {
        let mut tracker = PPSClock::new();

        let mut clock = 2349837;
        tracker.last_pps(clock);

        clock += 32_000_000;

        tracker.last_pps(clock);

        clock += 1_000_000;

        let nano_sec = tracker.to_ns(clock);
        assert!(nano_sec.is_ok());
        assert_eq!(031_250_000, nano_sec.unwrap());
    }

    #[test]
    fn test_wrap() {
        let mut tracker = PPSClock {
            last: Some(u32::MAX - 2_000_000),
            clock_per_sec: 32_000_000,
        };

        let clock = tracker.last.unwrap().wrapping_add(8_000_000);
        let nano_sec = tracker.to_ns(clock);
        assert!(nano_sec.is_ok());
        assert_eq!(250_000_000, nano_sec.unwrap());
    }

    #[test]
    fn test_past() {
        let mut tracker = PPSClock {
            last: Some(u32::MAX - 2_000_000),
            clock_per_sec: 32_000_000,
        };

        let clock = u32::MAX - 4_000_000;
        let nano_sec = tracker.to_ns(clock);
        assert!(nano_sec.is_ok());
        assert_eq!(-062_500_000, nano_sec.unwrap());
    }
}
