use core::{
    fmt::{self, Display},
    time::Duration,
};

use time::{macros::datetime, OffsetDateTime};
use ufmt::{uDisplay, uwrite};

use serde::{
    de::{self, MapAccess, Visitor},
    ser::SerializeMap,
    Deserialize, Serialize,
};

const SECS_PER_WEEK: u64 = 60 * 60 * 24 * 7;

#[derive(Debug, Clone, Copy)]
pub struct GPSTime(Duration);

impl GPSTime {
    pub const GPS_EPOCH: OffsetDateTime = datetime!(1980-01-06 0:00 UTC);

    pub const fn new(week: u16, mut secs: u64, nanos: u32) -> Self {
        secs += week as u64 * SECS_PER_WEEK;
        Self(Duration::new(secs, nanos))
    }

    pub fn to_datetime(&self) -> OffsetDateTime {
        GPSTime::GPS_EPOCH + self.0
    }

    pub fn as_duration(&self) -> Duration {
        self.0
    }

    pub const fn saturating_add_nanos(self, nanos: i32) -> GPSTime {
        if nanos > 0 {
            GPSTime(self.0.saturating_add(Duration::new(0, nanos as u32)))
        } else {
            GPSTime(self.0.saturating_sub(Duration::new(0, -nanos as u32)))
        }
    }

    // /// Checked `GPSTime` addition. Computes `self + other`, returning [`None`]
    // /// if overflow occurred.
    // pub const fn checked_add(self, mut rhs: GPSTime) -> Option<GPSTime> {

    //     let mut nano = self.nano + rhs.nano;
    //     if nano > NANOS_PER_SEC {
    //         nano -= NANOS_PER_SEC;
    //         rhs.second += 1;
    //     }
    //     debug_assert!(nano < NANOS_PER_SEC);

    //     let mut second = self.second + rhs.second;
    //     if second > GPSTime::SECS_PER_WEEK {
    //         second -= GPSTime::SECS_PER_WEEK;
    //         rhs.week += 1;
    //     }
    //     debug_assert!(second < GPSTime::SECS_PER_WEEK);

    //     if let Some(week) = self.week.checked_add(rhs.week) {
    //         Some(GPSTime{
    //             week,
    //             second,
    //             nano,
    //         })
    //     } else {
    //         None
    //     }

    // }
}

impl Display for GPSTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let datetime = self.to_datetime();
        write!(f, "{}T{}", datetime.date(), datetime.time())
    }
}

impl Serialize for GPSTime {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(2))?;
        map.serialize_entry("secs", &self.0.as_secs())?;
        map.serialize_entry("nanos", &self.0.subsec_nanos())?;
        map.end()
    }
}

impl<'de> Deserialize<'de> for GPSTime {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct DurationVisitor;

        impl<'de> Visitor<'de> for DurationVisitor {
            type Value = Duration;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct Duration")
            }

            fn visit_map<V>(self, mut map: V) -> Result<Duration, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut secs = None;
                let mut nanos = None;
                while let Some(key) = map.next_key()? {
                    match key {
                        "secs" => {
                            if secs.is_some() {
                                return Err(de::Error::duplicate_field("secs"));
                            }
                            secs = Some(map.next_value()?);
                        }
                        "nanos" => {
                            if nanos.is_some() {
                                return Err(de::Error::duplicate_field("nanos"));
                            }
                            nanos = Some(map.next_value()?);
                        }
                        _ => {}
                    }
                }
                let secs = secs.ok_or_else(|| de::Error::missing_field("secs"))?;
                let nanos = nanos.ok_or_else(|| de::Error::missing_field("nanos"))?;
                Ok(Duration::new(secs, nanos))
            }
        }

        let duration = deserializer.deserialize_map(DurationVisitor)?;
        Ok(GPSTime(duration))
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct WGS84Position {
    /// longitude (deg) 1e-7 scale
    pub lon: i32,

    /// latitude (deg) 1e-7 scale
    pub lat: i32,

    /// Height above ellipsoid (mm)
    pub height: i32,

    /// Horizontal accuracy estimate (mm)
    pub hacc: u32,

    /// Vertical accuracy estimate (mm)
    pub vacc: Option<u32>,
}

pub struct Position3D {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

// impl serde::Serialize for WGS84Position {
//     fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
//     where S: serde::Serializer
//     {
//         let mut m = serializer.serialize_map(None)?;
//         m.serialize_entry("lon", &self.lon)?;
//         m.serialize_entry("lat", &self.lat)?;
//         m.serialize_entry("height", &self.height)?;
//         m.serialize_entry("hacc", &self.hacc)?;
//         m.serialize_entry("vacc", &self.vacc)?;
//         m.end()
//     }
// }

impl uDisplay for WGS84Position {
    fn fmt<W>(&self, formatter: &mut ufmt::Formatter<'_, W>) -> Result<(), W::Error>
    where
        W: ufmt::uWrite + ?Sized,
    {
        fn to_decimal(value: i32) -> (i32, i32) {
            (value / 10_000_000_i32, value.abs() % 10_000_000_i32)
        }

        let lat = to_decimal(self.lat);
        let lon = to_decimal(self.lon);
        uwrite!(formatter, "[{}.{}, {}.{}]", lat.0, lat.1, lon.0, lon.1)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_gpstime_display() {
        let gps_time = GPSTime::new(2000, 300, 23498725);
        let display_str = gps_time.to_string();
        assert_eq!("2018-05-06T0:05:00.023498725", display_str);
    }
}
