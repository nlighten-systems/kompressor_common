use crate::lib::*;

pub struct Tracer {
    trace: SmallString,
}

impl Tracer {
    pub fn new<T: AsRef<str>>(trace: T) -> Self {
        let trace = SmallString::from(trace.as_ref());
        trace!("start {}", trace.as_str());
        //let _ = uwrite!(writer.borrow_mut(), "start {}\r\n", trace.as_str());

        Self { trace }
    }
}

impl Drop for Tracer {
    fn drop(&mut self) {
        //let _ = uwrite!(self.writer.borrow_mut(), "end {}\r\n", self.trace.as_str());
        trace!("end {}", self.trace.as_str());
    }
}
