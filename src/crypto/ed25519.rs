use core::ops::Deref;

pub const ED25519_PUBLIC_KEY_SIZE: usize = 32;
pub const ED25519_PRIVATE_KEY_SIZE: usize = 64;
pub const ED25519_SIGNATURE_SIZE: usize = 64;

//pub type PublicKey = [u8 ; ED25519_PUBLIC_KEY_SIZE];
//pub type PrivateKey = [u8 ; ED25519_PRIVATE_KEY_SIZE];
pub type Signature = [u8; ED25519_SIGNATURE_SIZE];

pub struct KeyPair {
    pub public: PublicKey,
    pub private: PrivateKey,
}

extern "C" {
    fn compact_ed25519_verify(
        signature: *const u8,
        public_key: *const u8,
        message: *const u8,
        msg_length: cty::size_t,
    ) -> bool;
    fn compact_ed25519_keygen(private_key: *mut u8, public_key: *mut u8, random_seed: *const u8);
    fn compact_ed25519_sign(
        signature: *mut u8,
        private_key: *const u8,
        message: *const u8,
        msg_length: cty::size_t,
    );
}

pub struct PublicKey([u8; ED25519_PUBLIC_KEY_SIZE]);

impl PublicKey {
    pub fn new(data: &[u8; ED25519_PUBLIC_KEY_SIZE]) -> Self {
        Self(*data)
    }

    pub fn verify(&self, message: impl AsRef<[u8]>, sig: &Signature) -> bool {
        let message = message.as_ref();
        unsafe {
            compact_ed25519_verify(
                sig.as_ptr(),
                self.0.as_ptr(),
                message.as_ptr(),
                message.len(),
            )
        }
    }
}

impl Deref for PublicKey {
    type Target = [u8; ED25519_PUBLIC_KEY_SIZE];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct PrivateKey([u8; ED25519_PRIVATE_KEY_SIZE]);

impl PrivateKey {
    pub fn new(data: &[u8; ED25519_PRIVATE_KEY_SIZE]) -> Self {
        Self(*data)
    }

    pub fn sign(&self, message: impl AsRef<[u8]>) -> Signature {
        let mut signature = [0_u8; ED25519_SIGNATURE_SIZE];
        let message = message.as_ref();
        unsafe {
            compact_ed25519_sign(
                signature.as_mut_ptr(),
                self.0.as_ptr(),
                message.as_ptr(),
                message.len(),
            );
        }
        signature
    }
}

impl Deref for PrivateKey {
    type Target = [u8; ED25519_PRIVATE_KEY_SIZE];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[inline]
pub fn ed25519_keygen(seed: [u8; 32]) -> KeyPair {
    let mut public = [0_u8; ED25519_PUBLIC_KEY_SIZE];
    let mut private = [0_u8; ED25519_PRIVATE_KEY_SIZE];

    unsafe {
        compact_ed25519_keygen(private.as_mut_ptr(), public.as_mut_ptr(), seed.as_ptr());
    }
    KeyPair {
        public: PublicKey(public),
        private: PrivateKey(private),
    }
}

#[inline]
pub fn ed25519_sign(message: &[u8], private_key: &PrivateKey) -> Signature {
    let mut signature = [0_u8; ED25519_SIGNATURE_SIZE];
    unsafe {
        compact_ed25519_sign(
            signature.as_mut_ptr(),
            private_key.as_ptr(),
            message.as_ptr(),
            message.len(),
        );
    }
    signature
}

pub fn ed25519_verify(
    sig: &Signature,
    public_key: &[u8; ED25519_PUBLIC_KEY_SIZE],
    message: &[u8],
) -> bool {
    unsafe {
        compact_ed25519_verify(
            sig.as_ptr(),
            public_key.as_ptr(),
            message.as_ptr(),
            message.len(),
        )
    }
}
