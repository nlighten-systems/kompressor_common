extern "C" {
    fn sha512_block(state: *mut sha512_state, blk: *const u8);
    fn sha512_final(state: *mut sha512_state, blk: *const u8, total_size: cty::size_t);
    fn sha512_get(
        state: *const sha512_state,
        hash: *mut cty::uint8_t,
        offset: cty::c_uint,
        len: cty::c_uint,
    );

    static sha512_initial_state: sha512_state;
}

#[repr(C)]
#[derive(Copy, Clone)]
struct sha512_state {
    h: [cty::uint64_t; 8],
}

pub struct Sha512 {
    state: sha512_state,
    block: [u8; Sha512::SHA512_BLOCK_SIZE],
    size: usize,
}

impl Sha512 {
    pub const SHA512_BLOCK_SIZE: usize = 128;
    pub const SHA512_HASH_SIZE: usize = 64;

    pub fn new() -> Self {
        Sha512 {
            state: unsafe { sha512_initial_state },
            block: [0; Sha512::SHA512_BLOCK_SIZE],
            size: 0,
        }
    }

    pub fn write_byte(&mut self, b: u8) {
        self.block[self.size % Sha512::SHA512_BLOCK_SIZE] = b;
        self.size += 1;
        if self.size == Sha512::SHA512_BLOCK_SIZE {
            unsafe {
                sha512_block(&mut self.state, self.block.as_ptr());
            }
        }
    }

    pub fn write(&mut self, bytes: &[u8]) {
        for b in bytes.iter() {
            self.write_byte(*b);
        }
    }

    pub fn finish(mut self) -> [u8; Sha512::SHA512_HASH_SIZE] {
        unsafe {
            sha512_final(&mut self.state, self.block.as_ptr(), self.size);
        }
        let mut hash = [0_u8; Sha512::SHA512_HASH_SIZE];
        unsafe {
            sha512_get(
                &self.state,
                hash.as_mut_ptr(),
                0,
                Sha512::SHA512_HASH_SIZE as u32,
            );
        }
        hash
    }
}

#[cfg(feature = "serde_cbor")]
impl serde_cbor::ser::Write for Sha512 {
    type Error = serde_cbor::Error;

    fn write_all(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        self.write(buf);
        Ok(())
    }
}

impl crate::io::Write for Sha512 {
    fn write(&mut self, buf: &[u8]) -> crate::io::Result<usize> {
        self.write(buf);
        Ok(buf.len())
    }
}
