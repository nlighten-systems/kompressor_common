pub enum ErrorKind {
    Unknown,
    Cancel,
    TimedOut,
}

pub struct Error {
    pub kind: ErrorKind,
}

impl From<ErrorKind> for Error {
    fn from(k: ErrorKind) -> Self {
        Error { kind: k }
    }
}

pub type Result<T> = core::result::Result<T, Error>;

pub trait Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
    fn read_exact(&mut self, buf: &mut [u8]) -> Result<()> {
        default_read_exact(self, buf)
    }
}

pub(crate) fn default_read_exact<R>(this: &mut R, mut buf: &mut [u8]) -> Result<()>
where
    R: Read + ?Sized,
{
    while !buf.is_empty() {
        match this.read(buf) {
            Ok(0) => break,
            Ok(n) => {
                //let tmp = buf;
                //buf = &mut tmp[n..];
                buf = &mut buf[n..];
            }
            Err(e) => return Err(e),
        }
    }
    Ok(())
}

pub trait Write {
    fn write(&mut self, buf: &[u8]) -> Result<usize>;
    fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        default_write_all(self, buf)
    }
}

pub(crate) fn default_write_all<W>(this: &mut W, mut buf: &[u8]) -> Result<()>
where
    W: Write + ?Sized,
{
    while !buf.is_empty() {
        match this.write(buf) {
            Ok(0) => break,
            Ok(n) => {
                buf = &buf[n..];
            }
            Err(e) => return Err(e),
        }
    }
    Ok(())
}

#[cfg(feature = "alloc")]
impl Write for bytes::BytesMut {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        use bytes::{Buf, BufMut};

        self.put(buf);
        Ok(buf.len())
    }
}
