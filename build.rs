fn main() {
    cc::Build::new()
        .file("compact25519/src/compact_ed25519.c")
        .file("compact25519/src/compact_wipe.c")
        .file("compact25519/src/c25519/c25519.c")
        .file("compact25519/src/c25519/ed25519.c")
        .file("compact25519/src/c25519/edsign.c")
        .file("compact25519/src/c25519/f25519.c")
        .file("compact25519/src/c25519/fprime.c")
        .file("compact25519/src/c25519/sha512.c")
        .compile("compact25519");
}
